package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"gitlab.com/Hugrid-1/parser-dockercompose/config"
	"gitlab.com/Hugrid-1/parser-dockercompose/controller"
	"gitlab.com/Hugrid-1/parser-dockercompose/models/vacancy/repository"
	"gitlab.com/Hugrid-1/parser-dockercompose/models/vacancy/service"
	"gitlab.com/Hugrid-1/parser-dockercompose/router"
	"gitlab.com/Hugrid-1/parser-dockercompose/server/httpserver"
	"gitlab.com/Hugrid-1/parser-dockercompose/vacancyParser/parser"
	parserService "gitlab.com/Hugrid-1/parser-dockercompose/vacancyParser/service"
	"log"
	"os"
	"os/signal"
)

func main() {
	var err error
	err = godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	// config
	appConfig := config.NewAppConf()
	fmt.Println(appConfig)

	// repository
	vacancyRepository, err := repository.NewVacancyStorage(appConfig)
	if err != nil {
		log.Fatal("Repository creating error ", err)
	}
	vacancyParser := parser.NewParser()

	// service
	vacancyService := service.NewVacancyService(vacancyRepository)
	parseService := parserService.NewParserService(vacancyParser)

	//controllers
	vacancyController := controller.NewVacancyController(vacancyService)
	parserController := controller.NewParserController(vacancyService, parseService)

	// start and route http server
	vacancyRouter := router.NewRouter(vacancyController, parserController)
	vacancyRouter.Swagger()
	vacancyRouter.VacancyAPI()
	vacancyRouter.ParserAPI()

	server := httpserver.NewHTTPServer(appConfig.Server, vacancyRouter)

	//selenium driver quit
	defer vacancyParser.Quit()

	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	err = server.Shutdown()
	if err != nil {
		fmt.Println(err)
	}
}
