package router

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/Hugrid-1/parser-dockercompose/controller"
	"net/http"
)

type Router struct {
	*chi.Mux
	vacancyController *controller.VacancyController
	parserController  *controller.ParserController
}

func NewRouter(vacancyController *controller.VacancyController, parserController *controller.ParserController) *Router {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("public"))).ServeHTTP(w, r)
	})

	return &Router{
		Mux:               r,
		vacancyController: vacancyController,
		parserController:  parserController,
	}
}

func (r *Router) ParserAPI() {
	r.Post("/search", r.parserController.VacancySearch)
}
func (r *Router) VacancyAPI() {
	r.Post("/delete", r.vacancyController.VacancyDelete)
	r.Post("/get", r.vacancyController.VacancyGet)
	r.Get("/list", r.vacancyController.VacancyGetList)
}

func (r *Router) Swagger() {
	r.Get("/swagger", swaggerUI)
}
