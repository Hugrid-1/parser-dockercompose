package service

import (
	"gitlab.com/Hugrid-1/parser-dockercompose/models/vacancy/models"
)

type VacancyRepository interface {
	Create(dto models.Vacancy) error
	GetByID(id int) (models.Vacancy, error)
	GeList() ([]models.Vacancy, error)
	Delete(id int) error
}

type VacancyService interface {
	AddVacancy(dto models.Vacancy) error
	GetVacancyByID(id int) (models.Vacancy, error)
	ListVacancy() ([]models.Vacancy, error)
	RemoveVacancy(id int) error
}

type service struct {
	vacancyRepository VacancyRepository
}

func NewVacancyService(storage VacancyRepository) VacancyService {
	return &service{storage}
}

func (s service) AddVacancy(dto models.Vacancy) error {
	err := s.vacancyRepository.Create(dto)
	return err
}

func (s service) GetVacancyByID(id int) (models.Vacancy, error) {
	byID, err := s.vacancyRepository.GetByID(id)
	if err != nil {
		return models.Vacancy{}, err
	}
	return byID, err
}

func (s service) ListVacancy() ([]models.Vacancy, error) {
	resultList, err := s.vacancyRepository.GeList()
	return resultList, err
}

func (s service) RemoveVacancy(id int) error {
	err := s.vacancyRepository.Delete(id)
	return err
}
