package models

import "encoding/json"

func UnmarshalVacancy(data []byte) (Vacancy, error) {
	var r Vacancy
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Vacancy) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type Vacancy struct {
	ID          int    `json:"id" db:"id" bson:"id,omitempty"`
	DatePosted  string `json:"datePosted" db:"publish_date" bson:"datePosted"`
	Title       string `json:"title" db:"title" bson:"title"`
	Url         string `db:"url" bson:"url"`
	Description string `json:"description" db:"description" bson:"description"`
	//Identifier         `json:"identifier"`
	HiringOrganization `json:"hiringOrganization"`
	EmploymentType     string `json:"employmentType" db:"employment_type" bson:"employmentType"`
}

type HiringOrganization struct {
	Name   string `json:"name" db:"company"`
	Logo   string `json:"logo" db:"company_logo_url"`
	SameAs string `json:"sameAs" db:"company_url"`
}

type Identifier struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

// Убрано временно (Потом доработать)!
//type JobLocation struct {
//	Type    string  `json:"@type"`
//	Address Address `json:"address"`
//}

//type Address struct {
//	Type            string         `json:"@type"`
//	StreetAddress   string         `json:"streetAddress"`
//	AddressLocality string         `json:"addressLocality"`
//	AddressCountry  AddressCountry `json:"addressCountry"`
//}
//
//type AddressCountry struct {
//	Type string `json:"@type"`
//	Name string `json:"name" db:"country"`
//}
