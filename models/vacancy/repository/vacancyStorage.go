package repository

import (
	"fmt"
	"gitlab.com/Hugrid-1/parser-dockercompose/config"
	"gitlab.com/Hugrid-1/parser-dockercompose/db"
	"gitlab.com/Hugrid-1/parser-dockercompose/models/vacancy/models"
	"gitlab.com/Hugrid-1/parser-dockercompose/models/vacancy/repository/nosql"
	"gitlab.com/Hugrid-1/parser-dockercompose/models/vacancy/repository/sql"
	"os"
)

type VacancyStorager interface {
	Create(dto models.Vacancy) error
	GetByID(id int) (models.Vacancy, error)
	GeList() ([]models.Vacancy, error)
	Delete(id int) error
}

func NewVacancyStorage(appConfig config.AppConfig) (VacancyStorager, error) {
	dbType := os.Getenv("DB_TYPE")
	if dbType == "" {
		return nil, fmt.Errorf("не указана переменная окружения DB_TYPE")
	}

	switch dbType {
	case "postgres":
		postgresDB, err := db.NewSqlDB(appConfig.DB)
		if err != nil {
			return nil, err
		}
		storage := sql.NewPostgresStorage(postgresDB)
		return storage, nil

	case "mongodb":
		mongoDB := db.NewMongoCollection(appConfig.MongoDB)
		storage, err := nosql.NewMongoDBStorage(mongoDB)
		if err != nil {
			return nil, err
		}
		return storage, nil

	default:
		return nil, fmt.Errorf("неизвестный тип базы данных: %s", dbType)
	}
}
