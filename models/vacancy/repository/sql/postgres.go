package sql

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/Hugrid-1/parser-dockercompose/models/vacancy/models"
)

type PostgresStorage struct {
	db *sqlx.DB
}

func NewPostgresStorage(db *sqlx.DB) *PostgresStorage {
	return &PostgresStorage{
		db: db,
	}
}

func (v *PostgresStorage) Create(dto models.Vacancy) error {
	query := `
		INSERT INTO vacancies (title, description, url, company,company_url, company_logo_url, employment_type,publish_date)
		VALUES ($1, $2, $3, $4, $5,$6,$7,$8)
		RETURNING id
	`
	result, err := v.db.Query(query,
		dto.Title, dto.Description, dto.Url,
		dto.HiringOrganization.Name,
		dto.HiringOrganization.SameAs,
		dto.HiringOrganization.Logo,
		dto.EmploymentType, dto.DatePosted)
	if err != nil {
		return err
	}

	if result.Next() {
		err := result.Scan(&dto.ID)
		if err != nil {
			return err
		}
	}

	return nil
}

func (v *PostgresStorage) GetByID(id int) (models.Vacancy, error) {
	var vacancy models.Vacancy
	query := `
		SELECT * FROM vacancies
		WHERE id = $1
	`
	udb := v.db.Unsafe()
	err := udb.Get(&vacancy, query, id)
	if err != nil {
		return models.Vacancy{}, err
	}
	return vacancy, nil
}

func (v *PostgresStorage) GeList() ([]models.Vacancy, error) {
	vacancies := make([]models.Vacancy, 0)
	query := `
		SELECT * FROM vacancies
	`
	udb := v.db.Unsafe()
	err := udb.Select(&vacancies, query)
	return vacancies, err
}

func (v *PostgresStorage) Delete(id int) error {
	query := `
		DELETE FROM vacancies
		WHERE id = $1
	`
	_, err := v.db.Exec(query, id)
	if err != nil {
		return err
	}
	return err
}
