package nosql

import (
	"fmt"
	"gitlab.com/Hugrid-1/parser-dockercompose/models/vacancy/models"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type MongoDBStorage struct {
	collection *mgo.Collection
}

func NewMongoDBStorage(collection *mgo.Collection) (*MongoDBStorage, error) {
	return &MongoDBStorage{
		collection: collection,
	}, nil
}

func (ms *MongoDBStorage) Create(vacancy models.Vacancy) error {
	err := ms.collection.Insert(vacancy)
	if err != nil {
		return err
	}

	fmt.Println("Вакансия успешно создана:", vacancy)

	return nil
}

func (ms *MongoDBStorage) Delete(id int) error {
	err := ms.collection.Remove(bson.M{"id": id})
	if err != nil {
		return err
	}

	fmt.Println("Вакансия успешно удалена")

	return nil
}

func (ms *MongoDBStorage) GetByID(id int) (models.Vacancy, error) {
	var vacancy models.Vacancy
	err := ms.collection.Find(bson.M{"id": id}).One(&vacancy)
	if err != nil {
		return models.Vacancy{}, err
	}

	return vacancy, nil
}
func (ms *MongoDBStorage) GeList() ([]models.Vacancy, error) {
	vacancies := make([]models.Vacancy, 0)
	ms.collection.Find(&vacancies)
	return vacancies, nil
}
