package config

import (
	"fmt"
	"os"
	"strconv"
	"time"
)

type AppConfig struct {
	Server  Server
	DB      DB
	MongoDB Mongo
}

type Server struct {
	Port            string        `yaml:"port"`
	ShutdownTimeout time.Duration `yaml:"shutdown_timeout"`
}

type DB struct {
	Net      string `yaml:"net"`
	Driver   string `yaml:"driver"`
	Name     string `yaml:"name"`
	User     string `json:"-" yaml:"user"`
	Password string `json:"-" yaml:"password"`
	Host     string `yaml:"host"`
	MaxConn  int    `yaml:"max_conn"`
	Port     string `yaml:"port"`
	Timeout  int    `yaml:"timeout"`
}

type Mongo struct {
	User     string
	Password string
	Host     string
	Port     string
	Name     string
}

func NewAppConf() AppConfig {
	maxConnStr := os.Getenv("MAX_CONN")
	maxConn, err := strconv.Atoi(maxConnStr)
	if err != nil {
		fmt.Println(err)
	}
	timeoutStr := os.Getenv("DB_TIMEOUT")
	timeout, err := strconv.Atoi(timeoutStr)
	if err != nil {
		fmt.Println(err)
	}
	return AppConfig{
		Server: Server{
			Port: os.Getenv("SERVER_PORT"),
		},
		DB: DB{
			Net:      os.Getenv("DB_NET"),
			Driver:   os.Getenv("DB_DRIVER"),
			Name:     os.Getenv("DB_NAME"),
			User:     os.Getenv("DB_USER"),
			Password: os.Getenv("DB_PASSWORD"),
			Host:     os.Getenv("DB_HOST"),
			MaxConn:  maxConn,
			Port:     os.Getenv("DB_PORT"),
			Timeout:  timeout,
		},
		MongoDB: Mongo{
			User:     os.Getenv("MONGO_USER"),
			Password: os.Getenv("MONGO_PASSWORD"),
			Host:     os.Getenv("MONGO_HOST"),
			Port:     os.Getenv("MONGO_PORT"),
			Name:     os.Getenv("MONGO_NAME"),
		},
	}
}
