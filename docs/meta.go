// Package classification  vacancy-Parser.
//
// Documentation of auto trade API.
//
//	Schemes:
//	- server.go
//	- https
//	BasePath: /
//	Version: 1.0.0
//
//	Consumes:
//	- application/json
//	- multipart/form-data
//
//	Produces:
//	- application/json
//
// swagger:meta
package docs

//go:generate swagger generate spec -o ../public/swagger.json --scan-models
