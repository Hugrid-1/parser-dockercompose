package docs

import (
	"gitlab.com/Hugrid-1/parser/controller"
	"gitlab.com/Hugrid-1/parser/models/vacancy/models"
)

// swagger:route POST /search vacancy vacancySearchRequest
// Парсинг вакансий и добавление их в базу данных.

//swagger:parameters vacancySearchRequest
type vacancySearchRequest struct {
	// in:body
	Body controller.SearchRequest
}

// swagger:route POST /get vacancy vacancyGetByIDRequest
// Получить вакансию по ID
// responses:
//	  200: vacancyGetByIDResponse

//swagger:parameters vacancyGetByIDRequest
type vacancyGetByIDRequest struct {
	// in:body
	Body controller.VacancyIDRequest
}

// swagger:response vacancyGetByIDResponse
type vacancyGetByIDResponse struct {
	// in:body
	Body models.Vacancy
}

// swagger:route GET /list vacancy vacancyGetListRequest
// Получить все вакансии.
// responses:
//	  200: vacancyGetListResponse

//swagger:parameters vacancyGetListRequest
type vacancyGetListRequest struct {
}

// swagger:response vacancyGetListResponse
type vacancyGetListResponse struct {
	// in:body
	Body []models.Vacancy
}

// swagger:route POST /delete vacancy vacancyDeleteRequest
// Удалить вакансию

//swagger:parameters vacancyDeleteRequest
type vacancyDeleteRequest struct {
	// in:body
	Body controller.VacancyIDRequest
}
