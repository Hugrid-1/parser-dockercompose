package db

import (
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/Hugrid-1/parser-dockercompose/config"
	"io/ioutil"
	"time"
)

func NewSqlDB(dbConf config.DB) (*sqlx.DB, error) {
	var dsn string
	var err error
	var dbRaw *sql.DB

	switch dbConf.Driver {
	case "postgres":
		dsn = fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", dbConf.Host, dbConf.Port, dbConf.User, dbConf.Password, dbConf.Name)
	}

	ticker := time.NewTicker(1 * time.Second)
	defer ticker.Stop()
	timeoutExceeded := time.After(time.Second * time.Duration(dbConf.Timeout))

	for {
		select {
		case <-timeoutExceeded:
			return nil, fmt.Errorf("db connection failed after %d timeout %s", dbConf.Timeout, err)
		case <-ticker.C:
			dbRaw, err = sql.Open(dbConf.Driver, dsn)
			if err != nil {
				return nil, err
			}
			err = dbRaw.Ping()
			if err == nil {
				//migration
				db := sqlx.NewDb(dbRaw, dbConf.Driver)

				db.SetMaxOpenConns(50)
				db.SetMaxIdleConns(50)
				c, ioErr := ioutil.ReadFile("db/migrations/migrationQuery.sql")
				if ioErr != nil {
					return nil, err
				}
				migrateSql := string(c)
				_, err := db.Exec(migrateSql)
				if err != nil {
					return nil, err
				}
				return db, nil
			}
		}
	}
}
