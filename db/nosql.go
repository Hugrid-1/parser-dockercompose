package db

import (
	"fmt"
	"gitlab.com/Hugrid-1/parser-dockercompose/config"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/mgo.v2"
	"log"
	"time"
)

const (
	dsnFmt = "mongodb://%s:%s"
)

type MongoCollection struct {
	client *mongo.Client
	conf   config.DB
}

func NewMongoCollection(mongoConfig config.Mongo) *mgo.Collection {
	var collection *mgo.Collection
	dsn := fmt.Sprintf("mongodb://%s:%s", mongoConfig.Host, mongoConfig.Port)
	maxWait := time.Duration(5 * time.Second)
	session, err := mgo.DialWithTimeout(dsn, maxWait)
	if err == nil {
		collection = session.DB(mongoConfig.Name).C("vacancies")
	} else {
		log.Fatal("mongodb create client err ", err)
	}
	return collection
}
