create table if not exists vacancies
(
    id               bigserial
        constraint vacancies_pkey1
            primary key,
    title            varchar(233),
    description      varchar(12000),
    url              varchar(200),
    company          varchar(200),
    company_url      varchar(500),
    company_logo_url varchar(500),
    employment_type  varchar(55),
    publish_date     timestamp,
    created_at       timestamp default now(),
    updated_at       timestamp default now(),
    deleted_at       timestamp
);
alter table vacancies
    owner to postgres;

create index if not exists vacancies_created_at_idx
    on vacancies (created_at);

create index if not exists vacancies_deleted_at_idx
    on vacancies (deleted_at);

create unique index if not exists vacancies_pkey
    on vacancies (id);

create index if not exists vacancies_updated_at_idx
    on vacancies (updated_at);
