package controller

import (
	"encoding/json"
	"fmt"
	"gitlab.com/Hugrid-1/parser-dockercompose/models/vacancy/models"
	"net/http"
)

type SearchRequest struct {
	Query        string `json:"query"`
	MaxPageCount int    `json:"page_count"`
}

type ParserServicer interface {
	SearchVacancies(query string, pageForSearch int) ([]models.Vacancy, error)
}

type ParserController struct {
	vacancyService VacancyServicer
	parserService  ParserServicer
}

func NewParserController(vacancyService VacancyServicer, parserService ParserServicer) *ParserController {
	return &ParserController{
		vacancyService: vacancyService,
		parserService:  parserService,
	}
}

func (p *ParserController) VacancySearch(w http.ResponseWriter, r *http.Request) {
	var query SearchRequest
	err := json.NewDecoder(r.Body).Decode(&query)

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	vacancies, err := p.parserService.SearchVacancies(query.Query, query.MaxPageCount)
	for _, vacancy := range vacancies {
		err = p.vacancyService.AddVacancy(vacancy)
		if err != nil {
			fmt.Printf("ERROR : %v", err)
		}
	}

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(vacancies)                       // записываем результат vacancy json в server.go.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
