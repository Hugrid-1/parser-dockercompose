package controller

import (
	"encoding/json"
	"gitlab.com/Hugrid-1/parser-dockercompose/models/vacancy/models"
	"net/http"
)

type VacancyIDRequest struct {
	ID int `json:"id"`
}

type VacancyServicer interface {
	AddVacancy(dto models.Vacancy) error
	GetVacancyByID(id int) (models.Vacancy, error)
	ListVacancy() ([]models.Vacancy, error)
	RemoveVacancy(id int) error
}

type VacancyController struct {
	vacancyService VacancyServicer
}

func NewVacancyController(service VacancyServicer) *VacancyController {
	return &VacancyController{service}
}

func (v *VacancyController) VacancyGet(w http.ResponseWriter, r *http.Request) {
	var vacancyIDRequest VacancyIDRequest
	err := json.NewDecoder(r.Body).Decode(&vacancyIDRequest)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	vacancy, err := v.vacancyService.GetVacancyByID(vacancyIDRequest.ID) // создаем запись в нашем vacancyService
	if err != nil {                                                      // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(vacancy)

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (v *VacancyController) VacancyGetList(w http.ResponseWriter, r *http.Request) {
	vacancies, err := v.vacancyService.ListVacancy() // создаем запись в нашем vacancyService
	if err != nil {                                  // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(vacancies)                       // записываем результат vacancy json в server.go.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (v *VacancyController) VacancyDelete(w http.ResponseWriter, r *http.Request) {
	var vacancyIDRequest VacancyIDRequest
	err := json.NewDecoder(r.Body).Decode(&vacancyIDRequest)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = v.vacancyService.RemoveVacancy(vacancyIDRequest.ID) // удаляем запись в нашем vacancyService
	if err != nil {                                           // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8

}
