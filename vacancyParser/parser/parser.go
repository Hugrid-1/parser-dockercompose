package parser

import (
	"bytes"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
	"gitlab.com/Hugrid-1/parser-dockercompose/models/vacancy/models"
	"log"
	"math"
	"net/http"
	"regexp"
	"strconv"
	"time"
)

const (
	maxTries       = 10
	HabrCareerLink = "https://career.habr.com/"
)

type Parser struct {
	driver selenium.WebDriver
}

func NewParser() *Parser {
	parser := Parser{}
	return &parser
}

func (p *Parser) Quit() {
	p.driver.Quit()
}
func (p *Parser) Search(query string, pageForSearch int) ([]models.Vacancy, error) {
	var err error
	var pageCount int
	p.config()

	foundVacancies := make([]models.Vacancy, 0)

	// получение общего количества страниц
	err = p.driver.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", 1, query))
	pageCount, err = p.getPages()
	fmt.Printf("[LOG %v] Найдено %d страниц\n", time.Now().Format("15:04:05"), pageCount)
	if err != nil {
		return []models.Vacancy{}, err
	}

	if pageCount < pageForSearch {
		pageForSearch = pageCount
	}

	for pageNumber := 1; pageNumber <= pageForSearch; pageNumber++ {
		fmt.Printf("[LOG %v] Парсинг %v страницы:", time.Now().Format("15:04:05"), pageNumber)

		// Получение ссылок на вакансии
		err = p.driver.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", pageNumber, query))
		if err != nil {
			return []models.Vacancy{}, err
		}
		links, err := p.getLinks()

		if err != nil {
			return []models.Vacancy{}, err
		}

		//Получение вакансии из каждой полученной ссылки
		fmt.Printf("\n Обработка ссылок (%v):", len(links))
		for i, link := range links {
			vacancy, err := p.getVacancy(link)

			if err != nil {
				fmt.Println(err)
			}
			fmt.Printf("%v,", i+1)
			foundVacancies = append(foundVacancies, *vacancy)
		}
		fmt.Printf(";\n")
	}
	fmt.Printf("[LOG %v]Всего найдено и обработано вакансий %d\n", time.Now().Format("15:04:05"), len(foundVacancies))
	return foundVacancies, nil
}

func (p *Parser) config() {
	// прописываем конфигурацию для драйвера
	caps := selenium.Capabilities{
		"browserName": "chrome",
	}
	// добавляем в конфигурацию драйвера настройки для chrome
	chrCaps := chrome.Capabilities{
		W3C: true,
	}
	caps.AddChrome(chrCaps)
	// переменная нашего веб драйвера
	var wd selenium.WebDriver
	var err error
	// прописываем адрес нашего драйвера
	urlPrefix := "http://selenium:4444/wd/hub"
	// немного костылей чтобы драйвер не падал
	i := 1
	for i < maxTries {
		wd, err = selenium.NewRemote(caps, urlPrefix)
		if err != nil {
			log.Println(err)
			i++
			continue
		}
		break
	}
	p.driver = wd
}

func (p *Parser) getPages() (int, error) {
	elem, err := p.driver.FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		return 0, err
	}
	vacancyCountRaw, err := elem.Text()
	if err != nil {
		return 0, err
	}
	re := regexp.MustCompile("\\d{1,}")
	vacancyCountRaw = re.FindString(vacancyCountRaw)
	vacancyCount, err := strconv.Atoi(vacancyCountRaw)
	if err != nil {
		return 0, err
	}
	pageCount := int(math.Ceil(float64(vacancyCount) / 25))

	return pageCount, nil

}

func (p *Parser) getLinks() ([]string, error) {
	elems, err := p.driver.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
	if err != nil {
		return nil, err
	}
	var links []string
	for i := range elems {
		var link string
		link, err = elems[i].GetAttribute("href")
		if err != nil {
			continue
		}
		links = append(links, HabrCareerLink+link)
	}
	return links, nil
}

func (p *Parser) getVacancy(link string) (*models.Vacancy, error) {
	resp, err := http.Get(link)
	if err != nil {
		return &models.Vacancy{}, err
	}
	var doc *goquery.Document
	doc, err = goquery.NewDocumentFromReader(resp.Body)
	if err != nil && doc != nil {
		return &models.Vacancy{}, err
	}
	dd := doc.Find("script[type=\"application/ld+json\"]")
	if dd == nil {
		log.Println("Habr vacancy nodes not found")
		return &models.Vacancy{}, err
	}
	ss := dd.First().Text()
	re := regexp.MustCompile(`<(/?[^>]+)>`)
	resultString := re.ReplaceAllString(ss, " ")
	buf := bytes.NewBufferString(resultString)
	vacancy, err := models.UnmarshalVacancy(buf.Bytes())
	if err != nil {
		fmt.Println(err)
	}
	vacancy.Url = link
	return &vacancy, err
}
