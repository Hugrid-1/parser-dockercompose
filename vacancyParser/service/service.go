package service

import (
	"gitlab.com/Hugrid-1/parser-dockercompose/models/vacancy/models"
)

type Parserer interface {
	Search(query string, pageForSearch int) ([]models.Vacancy, error)
}

type ParserService struct {
	parser Parserer
}

func NewParserService(parser Parserer) *ParserService {
	return &ParserService{parser: parser}
}

func (p ParserService) SearchVacancies(query string, pageForSearch int) ([]models.Vacancy, error) {
	vacancies, err := p.parser.Search(query, pageForSearch)
	return vacancies, err
}
